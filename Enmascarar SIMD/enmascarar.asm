global enmascarar_asm

enmascarar_asm:

    push ebp
    mov ebp,esp
    xor ecx,ecx
    xor eax,eax
    
;se almacenan direcciones de imagenes, mascara y el tamaño
    mov ecx,[ebp+8]     
    mov ebx,[ebp+12]    
    mov edx,[ebp+16]    
    mov eax,[ebp+20]    
    
    jmp enmascara

enmascara:

    cmp eax,0           
    jl end              
;guarda en los registros los 8 bytes de las imagenes.
    movq mm0,qword[ecx] 
    movq mm1,qword[ebx]
    movq mm2,qword[edx] 
;compara los 8 bytes de las imagenes con la mascara. 
    pand mm1,mm2
    pandn mm2,mm0
    por mm1,mm2
;Mueve a ecx las modificaciones
    movq [ecx],mm1      
;Adiciona a los registros 8 bytes 
    add ecx,8           
    add ebx,8
    add edx,8
    sub eax,8           
    jmp enmascara

end:
    pop ebp
    ret