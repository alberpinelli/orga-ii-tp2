#include <stdio.h>
#include <stdlib.h>
//#include <string.h>

float enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mask, int cant);

void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int cant);

int main(int argc, char *argv[]){

    //Recibimos la ruta de las imagenes pasadas por argumento
    char* ruta_a = argv[1];
    char* ruta_b = argv[2];
    char* ruta_mask = argv[3];

    //Se crean punteros para las imagenes. Se abren las imagenes con la ruta correspondiente.   
    FILE *imagen1 = fopen(ruta_a,"rb");
    FILE *imagen2 = fopen(ruta_b,"rb");
    FILE *mascara = fopen(ruta_mask,"rb");

    //Determina la cantidad de posiciones que tiene el fichero imagen1.
    fseek(imagen1, SEEK_SET, SEEK_END);//Desplazamiento desde el primer byte del fichero al ultimo
    long cant = ftell(imagen1);//Cantidad de bytes del fichero
    rewind(imagen1); //Coloca el indicador de posición del fichero en la primer posición.

    //asigna bytes de memoria del tamaño de la imagen
    char *a = (char*) malloc(cant);
    char *b = (char*) malloc(cant);
    char *mask = (char*) malloc(cant);

    //Se leen las imagenes 
    fread(a,1,cant,imagen1);
    fread(b,1,cant,imagen2);
    fread(mask,1,cant,mascara);

    //funcion de enmascaramiento en assembler
    enmascarar_asm(a,b,mask,cant);

    //Se crean los archivos donde se almacenará la mascara aplicada
    FILE *salida_c = fopen("salida_c.rgb","wb");
    FILE *salida_asm = fopen("salida_asm.rgb","wb"); 
    
    //Se graba la mascara aplicada en el archivo
    fwrite(a,1,cant,salida_asm);
 
    //Funcion de enmascaramiento en C
    enmascarar_c(a,b,mask,cant);
    
    //Se graba la mascara aplicada en el archivo
    fwrite(a,1,cant,salida_c);

    //Se liberan punteros
    free(a);
    free(b);
    free(mask);

    //Se cierran archivos utilizados.
    fclose(imagen1);
    fclose(imagen2);
    fclose(mascara);
    fclose(salida_c);
    fclose(salida_asm);

    return 0;
}



//Funcion de enmascaramiento en C
void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int cant){
    
     char ch;
    
    for(long i=0;i<cant;i+=3){
        ch = ( char)mask[i]; 
        
        if(( int)ch == 255){ //verifica si el valor de CH corresponde al color blanco 255             
            a[i] = b[i];     //En caso de deserlo, reemplaza los valores de b en a.   
            a[i+1] = b[i+1];
            a[i+2] = b[i+2];
        }
    }
}
