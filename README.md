# UNIVERSIDAD NACIONAL GENERAL SARMIENTO | ORGANIZACIÓN DEL COMPUTADOR II 
# TRABAJO PRÁCTICO N°2 | ENMASCARAR CON SISMD

1ER CUATRIMETRE AÑO 2021
Alumno: Pinelli Raul Alberto

**DESCRIPCIÓN DEL TRABAJO**

En el presente trabajo, realiza una aplicación que combina dos imágenes usando una máscara de seleccion. La implementación de la misma se realiza en lenguaje **C** y **Ensamblador**.

**Implementación en C**.

-Para poder realizar el enmascaramiento, se recibe por consola la ruta de las imagenes y máscara. 
Desde C creamos punteros de archivos con las rutas correspondientes y nos encargamos de averiguar la cantidad de bytes que contiene cada imagen.

![Imagen 1](./Imagen1.png)

-Luego sabiendo la cantidad de bytes de cada imagnes, creamos punteros y les asignamos bytes de memoria del tamaño de la imagen correspondiente. Esto nos permitirá luego leer los archivos indicados. 

![Imagen 2](./Imagen2.png)
 
-Continuamos llamando a las funciones enmascarar_asm y enmascarar _c que seran las encargadas de realizar el reemplazo de los pixeles entre las dos imágenes ingresadas según los pixeles blancos de la mascara.
Para ello, es necesario que tambien creemos los archivos **salida_c** y **salida_asm** donde serán almacenadas las imagenes con la mascara aplicada.

![Imagen 3](./Imagen3.png)

-Para el proceso de enmascaramiento, se buscan los bytes de pixeles blancos en la máscara para sabér cual son los bytes de las imagen1 que seran reemplazados por los bytes de la imagen2.

![Imagen 5](./Imagen5.png)

-Por último, se realiza la liberación de los punteros utilizados y el cierre de los archivos utilizados.

![Imagen 4](./Imagen4.png)

**Implementación en Assembler**.

-Desde el lenguaje C se cargan en memoria las imagenes que serpan utilizadas en el enmascaramiento.

-En primer lugar, almacenamos en los registros ecx,ebx,edx y eax las direcciones de las dos imagenes, la mascara y la cantidad de byte de cada una. 

![Imagen 6](./Imagen6.png)

-Luego, cargamos en los registros MMX los bytes correspondientes del contenido de las imagenes.

| Registro | Imagen | 
| ------ | ------ |
| mm0 | imagen1 |
| mm1 | imagen2 |
| mm2 | mascara |

![Imagen 7](./Imagen7.png)

-Comenzamos el enmascaramiento comparando de a 8 bytes. Despues de haberlo comparado, aumentamos en 8 los registros hasta recorrer la imagen completa.

-Utilizando la instrucción lógica pand sobre la imagen2 y la máscara, se comparan los bytes y se obtienen los valores de la imagen2 que coinciden con pixeles blancos de la máscara.

-Utilizando la instrucción lógica pandn sobre la imagen1 y la máscara, se comparan los bytes y se obtienen los valores de la imagen1 que coinciden con pixeles negros de la máscara.

-Para finalizar el enmascaramiento, se unen los valores obtenidos anteriormente para construir la imagen enmascarada.

![Imagen 8](./Imagen8.png)


**Enmascaramiento Realizado**

-Haciendo uso del programa realizado en C y Assembler, podemos observar como se enmascaró la imagen de un paisaje con la de una persona.

![Imagen 9](./Paisaje.jpg)

![Imagen 10](./Persona.jpg)

![Imagen 11](./Mascara.jpg)

![Imagen 12](./ImagenEnmascarada.png)
